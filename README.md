This readme file will contain all of the command line commands issued and an explanation of what was done.

### 1) Raw read quality assessment in PrinSeq. ###
First generate the *.gd file

```
#!linux

perl /home/clowen/Hemiptera_Transcriptomes/prinseq-lite.pl -verbose -fastq reads.left.fastq -fastq2 reads.right.fastq -graph_data PackerPE.gd -out_good null -out_bad null
```

Now that we have the *.gd file, we can generate the .html and png files

```
#!linux

perl /groups/cbi/prinseq-lite-0.20.3/prinseq-graphs.pl -i /groups/packergroup/raw_seq/PackerPE.gd -png_all -html_all -log test_log.txt
```
### 2) Error correct raw reads using BLESS using a kmer length of 25 (the kmer length used in Trinity) ###

```
#!linux

mpirun -n 32 bless -read1 /groups/packergroup/raw_seq/reads.left.fastq -read2 /groups/packergroup/raw_seq/reads.right.fastq -prefix /groups/packergroup/raw_seq/bless_corrected -kmerlength 25
```


### 3) Adapter trimming of quality assigned reads using Trimmomatic 0.32 ###

Using custom adapter file of Illumina primers, adapters, PCR fragments, etc.

```
#!linux

java -jar $TrinityPATH/trinity-plugins/Trimmomatic/trimmomatic-0.32.jar PE -threads 12 -phred33 -trimlog /home/simonwentworth/Trim.log $raw_seqPATH/bless_corrected.1.corrected.fastq $raw_seqPATH/bless_corrected.2.corrected.fastq $raw_seqPATH/first.left1.fastq $raw_seqPATH/first.left2.fastq $raw_seqPATH/first.right1.fastq $raw_seqPATH/first.right2.fastq ILLUMINACLIP:/home/simonwentworth/custom_adapt:2:30:10 SLIDINGWINDOW:4:15 MINLEN:40
```

### 4) Quality control of trimmed reads using Fastx 0.0.6 ###

Remove any bases with a Phred score <30

```
#!linux

fastq_quality_filter -q 30 -v -i $raw_seqPATH/first.left1.fastq -Q33 -o $TrinInPATH/qual.left1.fq
fastq_quality_filter -q 30 -v -i $raw_seqPATH/first.left2.fastq -Q33 -o $TrinInPATH/qual.left2.fq
fastq_quality_filter -q 30 -v -i $raw_seqPATH/first.right1.fastq -Q33 -o $TrinInPATH/qual.right1.fq
fastq_quality_filter -q 30 -v -i $raw_seqPATH/first.right2.fastq -Q33 -o $TrinInPATH/qual.right2.fq
```

### 5) Filter Bacteria, mtDNA, Viruses, Fungi ###

This step uses bowtie2 to filter reads according to NCBI reference and custom databases.  We filtered reads for mtDNA (NCBI Cyprinidae mtDNA genomes), bacteria (NCBI nr bacteria database), viruses (NCBI nr virus database), ribosomal rRNA (Silva nr reference database), and fungi (NCBI nr fungal database).  We used the script python script *bowtie_filter_error_corrected_reads.py* to loop through each database and removed reads that mapped to any sequence in the databases.

```
#!linux

python bowtie_filter_error_corrected_reads.py qual.left1.fq qual.right1.fq singletons.fq fathead_bless /lustre/groups/cbi/refFiles cyprinidae_mtdna,bacteria_0,bacteria_1,bacteria_2,virus,Silva_Ref_NR,fungi_nt_ti

```

### 6) Assemble cleaned reads with Trinity ###
```
#!linux
Trinity --seqType fq --max_memory $MEMORY --left $TrinInPATH/left.fq $TrinInPATH/single.fq --right $TrinInPATH/right.fq --SS_lib_type RF --CPU $CPU --normalize_reads --output $TrinOutPATH
```

### 7) Map cleaned and filtered reads back to the assembly and get abundance profiles. ###
```
perl /c1/apps/trinity/r20140717/util/align_and_estimate_abundance.pl --thread_count 16 --transcripts /lustre/groups/packergroup/transcriptome_feb26/trinity_out/Trinity.fasta --seqType fq --left qual.left1.fq --right qual.right1.fq --est_method RSEM --aln_method bowtie2 --trinity_mode --prep_reference --output_dir /groups/packergroup/cleaned
```

### 8) Filter Trinity assembled transcripts by expression level ###
To remove poorly assembled transcripts that most likely represent artifacts, we removed transcripts that represent < 1% of the per-component expression level and < 0.5 transcripts per million (TPM).
```
#!linux
perl /c1/apps/trinity/r20140717/util/filter_fasta_by_rsem_values.pl --rsem_output=RSEM.isoforms.results --fasta=Trinity_final.fasta --tpm_cutoff=0.5 --isopct_cutoff=1.00 --output=Trinity_final_TPM05_ISO1.fasta
```
### 9) Translate contigs using Transdecoder ###
```
#!linux
$TrinityPATH/trinity-plugins/transdecoder/TransDecoder -t $TrinOutPATH/Trinity.fasta
```

### 10) Find BLAST homologies (both BLASTx and BLASTp) ###
```
#!linux
blastx -query $TrinOutPATH/Trinity.filtered.fasta -db $dbPATH/uniprot_sprot.trinotate.pep -num_threads $CPU -max_target_seqs 1 -outfmt 6 -evalue 1e-3 > $NoteInPATH/blastx_e-3.outfmt6

blastp -query $TrinOutPATH/Trinity.fasta.transdecoder.pep -db $dbPATH/uniprot_sprot.trinotate.pep -num_threads $CPU -max_target_seqs 1 -outfmt 6 -evalue 1e-3 > $NoteInPATH/blastp_e-3.outfmt6
```

### 11) Find conserved protein domains using HMMER ###
```
#!linux
hmmscan --cpu 8 --domtblout $NoteInPATH/TrinotatePFAM.out $dbPATH/Pfam-A.hmm $TrinOutPATH/Trinity.fasta.transdecoder.pep > $NoteInPATH/pfam.log
```

### 12) Find signal peptides using SignalP ###
```
#!linux
signalp -f short -n $NoteInPATH/signalp.out $TrinOutPATH/Trinity.fasta.transdecoder.pep
```

### 13) Predict transmembrane domains using tmhmm ###
```
#!linux
tmhmm --short < $TrinOutPATH/Trinity.fasta.transdecoder.pep > $NoteInPATH/tmhmm.out
```

### 14) Determine and unfiltered rRNA segments ###
```
#!linux
$TrinotatePATH/util/rnammer_support/RnammerTranscriptome.pl --transcriptome $TrinOutPATH/Trinity.filtered.fasta --path_to_rnammer /c1/apps/rnammer/rnammer
```

### 14) Perform Gene to Transcript Mapping ###
```
#!linux
$TrinityPATH/util/support_scripts/get_Trinity_gene_to_trans_map.pl  $TrinOutPATH/Trinity.filtered.fasta > $NoteInPATH/Trinity.fasta.gene_trans_map
```

### 15) Do FPKM estimation to determine quality metric for assembly ###
```
#!linux
perl $TrinityPATH/align_and_estimate_abundance.pl --transcripts $TrinOutPATH/Trinity.filtered.fasta --seqType fq --left $TrinInPATH/left.fq --right $TrinInPATH/right.fq --est_method RSEM --aln_method bowtie --SS_lib_type RF --thread_count 12 --output_dir $NoteInPATH --trinity_mode --prep_reference
```

### 16) Load annotation outputs into Trinotate SQlite database for simpler parsing ###
```
#!linux
Trinotate Trinotate.sqlite init --gene_trans_map $NoteInPATH/Trinity.fasta.gene_trans_map --transcript_fasta $TrinOutPATH/Trinity.filtered.fasta --transdecoder_pep $TrinOutPATH/Trinity.filtered.fasta.transdecoder.pep

#Blastp
Trinotate Trinotate.sqlite LOAD_swissprot_blastp $NoteInPATH/blastp.outfmt6

#Blastx
Trinotate Trinotate.sqlite LOAD_swissprot_blastx $NoteInPATH/blastx.outfmt6

#Pfam
Trinotate Trinotate.sqlite LOAD_pfam $NoteInPATH/TrinotatePFAM.out

#Transmembrane
Trinotate Trinotate.sqlite LOAD_tmhmm $NoteInPATH/tmhmm.out

#RNAMMER
Trinotate Trinotate.sqlite LOAD_rnammer $PATH2/Trinity.fasta.rnammer.gff

#Output
Trinotate Trinotate.sqlite report > trinotate_annotation_report.xls
```

### 17) Extract Gene Ontology Terms from annotation for easy parsing ###
```
#!linux
$TrinotatePATH/util/extract_GO_assignments_from_Trinotate_xls.pl --Trinotate_xls trinotate_annotation_report.xls -G --include_ancestral_terms > GO.txt
```

### 18) Determine amount of top blast hit that each matched transcript covers ###
```
#!linux
$TrinityPATH/util/analyze_blastPlus_topHit_coverage.pl $NoteInPATH/blastx.outfmt6 $TrinOutPATH/Trinity.filtered.fasta $dbPATH/uniprot_sprot.trinotate.pep
```